package com.goddy.oauth2.model.enums;

/**
 * @author Goddy
 * @date Create in 下午12:08 2018/6/25
 * @description
 */
public class Constant {

    public static final String SPLIT_COMMA = ",";

    public static final String DEFAULT_ACCESS_TOKEN_VALIDITY = "60";

    public static final String DEFAULT_REFRESH_TOKEN_VALIDITY = "600";

    public static final String RESOURCE_ID_NORMAL_APP = "wangcongming";
}
