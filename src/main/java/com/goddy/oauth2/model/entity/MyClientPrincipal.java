package com.goddy.oauth2.model.entity;

import com.goddy.oauth2.model.DO.Client;
import com.goddy.oauth2.model.enums.Constant;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.oauth2.provider.ClientDetails;


import java.util.*;

/**
 * @author Goddy
 * @date Create in 下午12:03 2018/6/25
 * @description
 */
public class MyClientPrincipal implements ClientDetails {

    //TODO:等待完善...

    private Client client;

    public MyClientPrincipal(Client client) {
        this.client = client;
    }

    @Override
    public String getClientId() {
        return client.getClientId();
    }

    @Override
    public Set<String> getResourceIds() {
        return new HashSet<>(Arrays.asList(client.getResourceIds().split(Constant.SPLIT_COMMA)));
    }

    @Override
    public boolean isSecretRequired() {
        return client.getSecretRequire();
    }

    @Override
    public String getClientSecret() {
        return client.getClientSecret();
    }

    @Override
    public boolean isScoped() {
        return client.getScopeRequire();
    }

    @Override
    public Set<String> getScope() {
        return new HashSet<>(Arrays.asList(client.getScope().split(Constant.SPLIT_COMMA)));
    }

    @Override
    public Set<String> getAuthorizedGrantTypes() {
        return new HashSet<>(Arrays.asList(client.getAuthorizedGrantTypes().split(Constant.SPLIT_COMMA)));
    }

    @Override
    public Set<String> getRegisteredRedirectUri() {
        return null;
    }

    @Override
    public Collection<GrantedAuthority> getAuthorities() {
        Collection<GrantedAuthority> collection = new ArrayList<>();
        Arrays.asList(client.getAuthorities().split(Constant.SPLIT_COMMA)).forEach(
                auth -> collection.add((GrantedAuthority) () -> auth)
        );
        return collection;
    }

    @Override
    public Integer getAccessTokenValiditySeconds() {
        return client.getAccessTokenValidity();
    }

    @Override
    public Integer getRefreshTokenValiditySeconds() {
        return client.getRefreshTokenValidity();
    }

    @Override
    public boolean isAutoApprove(String scope) {
        return false;
    }

    @Override
    public Map<String, Object> getAdditionalInformation() {
        return null;
    }
}
