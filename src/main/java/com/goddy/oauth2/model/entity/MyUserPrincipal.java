package com.goddy.oauth2.model.entity;

import com.goddy.oauth2.model.DO.User;
import com.goddy.oauth2.model.enums.Constant;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

/**
 * @author Goddy
 * @date Create in 下午12:11 2018/6/25
 * @description
 */
public class MyUserPrincipal implements UserDetails {

    //TODO:等待完善...

    private User user;

    public MyUserPrincipal(User user) {
        this.user = user;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        if (user.getAuthorities() == null) {
            return null;
        }
        Collection<GrantedAuthority> collection = new ArrayList<>();
        Arrays.asList(user.getAuthorities().split(Constant.SPLIT_COMMA)).forEach(
                auth -> collection.add((GrantedAuthority) () -> auth)
        );
        return collection;
    }

    @Override
    public String getPassword() {
        return user.getPassword();
    }

    @Override
    public String getUsername() {
        return user.getUsername();
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return user.getNoLocked();
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return user.getEnabled();
    }
}

