package com.goddy.oauth2.model.DO;

import lombok.Data;

import javax.persistence.*;

/**
 * @author Goddy
 * @date Create in 下午12:04 2018/6/25
 * @description
 */
@Data
@Entity
@Table(name = "oauth_client")
public class Client {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "client_id", unique = true)
    private String clientId;

    private String resourceIds;

    private String clientSecret;

    private Boolean secretRequire;

    private String scope;

    private Boolean scopeRequire;

    private String authorizedGrantTypes;

    private String authorities;

    private Integer accessTokenValidity;

    private Integer refreshTokenValidity;
}
