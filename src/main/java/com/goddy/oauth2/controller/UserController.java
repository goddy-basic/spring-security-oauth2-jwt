package com.goddy.oauth2.controller;

import com.goddy.oauth2.common.exception.MyException;
import com.goddy.oauth2.common.util.ResultUtil;
import com.goddy.oauth2.model.DO.User;
import com.goddy.oauth2.model.VO.Result;
import com.goddy.oauth2.model.enums.ResultEnum;
import com.goddy.oauth2.service.MyUserDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Goddy
 * @date Create in 上午9:27 2018/6/26
 * @description
 */
@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    private MyUserDetailsService userService;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @RequestMapping("/create")
    public User create(@RequestParam String username,
                       @RequestParam String password,
                       @RequestParam(required = false) String authorities) {
        User user = new User();
        user.setUsername(username);
        user.setPassword(passwordEncoder.encode(password));
        user.setEnabled(true);
        user.setNoLocked(true);
        user.setAuthorities(authorities);
        return userService.save(user);
    }

    @RequestMapping("/{username}")
    public Result<User> findByName(@PathVariable String username) {
        User user = userService.findByName(username);
        if (user == null) {
            throw new MyException(ResultEnum.FIND_USERNAME_NOT_EXIST);
        }
        return ResultUtil.success(user);
    }
}
