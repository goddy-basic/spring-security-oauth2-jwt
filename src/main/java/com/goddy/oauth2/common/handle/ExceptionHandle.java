package com.goddy.oauth2.common.handle;

import com.goddy.oauth2.common.exception.MyException;
import com.goddy.oauth2.common.util.ResultUtil;
import com.goddy.oauth2.model.VO.Result;
import com.goddy.oauth2.model.enums.ResultEnum;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author Goddy
 * @date Create in 上午10:10 2018/6/26
 * @description
 */
@Slf4j
@ControllerAdvice
public class ExceptionHandle {

    @ExceptionHandler(value = Exception.class)
    @ResponseBody
    public Result handle(Exception e) {

        if (e instanceof MyException) {
            MyException myException = (MyException) e;
            log.error(myException.toString());
            return ResultUtil.error(myException.getCode(), e.getMessage());
        } else if (e instanceof MissingServletRequestParameterException) {
            return ResultUtil.error(ResultEnum.MISS_REQUEST_PARAM_ERROR);
        } else {
            log.error("【系统异常】{}", e.toString());
            e.printStackTrace();
            return ResultUtil.error(-1, e.getMessage());
        }
    }
}
