package com.goddy.oauth2.service;

import com.goddy.oauth2.model.DO.User;
import org.springframework.security.core.userdetails.UserDetailsService;

/**
 * @author Goddy
 * @date Create in 下午12:13 2018/6/25
 * @description
 */
public interface MyUserDetailsService extends UserDetailsService {

    /**
     * 添加用户
     * @param user
     * @return
     */
    User save(User user);

    /**
     * 按名称查找
     * @param name
     * @return
     */
    User findByName(String name);
}
