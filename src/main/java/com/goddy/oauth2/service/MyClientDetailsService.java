package com.goddy.oauth2.service;

import com.goddy.oauth2.model.DO.Client;
import org.springframework.security.oauth2.provider.ClientDetailsService;

/**
 * @author Goddy
 * @date Create in 下午12:12 2018/6/25
 * @description
 */
public interface MyClientDetailsService extends ClientDetailsService {

    /**
     * 添加客户端
     * @param client
     * @return
     */
    Client save(Client client);

    Client findById(String clientId);
}

