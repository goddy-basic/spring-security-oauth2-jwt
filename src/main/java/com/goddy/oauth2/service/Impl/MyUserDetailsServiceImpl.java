package com.goddy.oauth2.service.Impl;

import com.goddy.oauth2.common.exception.MyException;
import com.goddy.oauth2.model.DO.User;
import com.goddy.oauth2.model.entity.MyUserPrincipal;
import com.goddy.oauth2.model.enums.ResultEnum;
import com.goddy.oauth2.repository.UserRepository;
import com.goddy.oauth2.service.MyUserDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

/**
 * @author Goddy
 * @date Create in 下午12:14 2018/6/25
 * @description
 */
@Service
public class MyUserDetailsServiceImpl implements MyUserDetailsService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userRepository.findByUsername(username);
        if (user == null) {
            throw new UsernameNotFoundException(username);
        }
        return new MyUserPrincipal(user);
    }

    @Override
    public User save(User user) {

        //1、如果已存在，则抛出异常
        if (userRepository.findByUsername(user.getUsername()) != null) {
            throw new MyException(ResultEnum.SAVE_USERNAME_EXIST_ERROR);
        }

        //2、否则，添加
        return userRepository.save(user);
    }

    @Override
    public User findByName(String name) {
        return userRepository.findByUsername(name);
    }
}
